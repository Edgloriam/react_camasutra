import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./components/header/header";
import SidePanel from "./components/side_panel/side_panel";
import Main from "./components/main/main";
import {BrowserRouter} from "react-router-dom";

function App(props) {
    return (
        <BrowserRouter>
            <div className="App">
                <Header/>
                <SidePanel/>
                <Main />
            </div>
        </BrowserRouter>
    );
}

export default App;
