import React from "react";
import classes from './Main.module.css'
import Profile from '../Profile/Profile';
import Music from '../Music/Music';
import UsersContainer from '../Users/UsersContainer';
import { Route} from 'react-router-dom';
import DialogsContainer from "../Dialogs/DialogsContainer";

const Main = (props) => {
    const extClasses = `${classes.main} bg`
    return (
        <div className={extClasses}>
            {/*<Posts />*/}
            <div className={classes.main__bg}></div>
            <Route path='/messages' render={ () => <DialogsContainer />} />
            <Route path='/profile' render={() => <Profile />} />
            <Route path='/music' component={Music} />
            <Route path='/users' component={() => <UsersContainer/>} />
        </div>
    )
}

export default Main;
