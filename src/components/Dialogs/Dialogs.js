import React from "react";
import style from './Dialogs.module.css';
import DialogItems from '../DialogsItems/DialogsItems';
import Messages from '../Messages/Messages';
import {Button} from "react-bootstrap";

const Dialogs = (props) => {
    const messagesElements = props.messagesData
        .map(msg => (<Messages msg={msg.msg} id={msg.id} key={`m_${msg.id}`}/>) );
    
    const dialogElements = props.dialogsData
        .map(dialog => (<DialogItems name={dialog.name} id={dialog.id} key={`d_${dialog.id}`}/>) );
    
    const addNewMsgText = React.createRef();
    
    const addMsgHandler = () => {
        const text = addNewMsgText.current.value;
        props.addMsgActionCreator(text);
    }
    
    const onChangeHandler = () => {
        const newText = addNewMsgText.current.value;
        props.updateNewMessageActionCreator(newText);
    }
    return (
        <div className={style.dialogs}>
            <div className={style.dialogs__items}>
                {dialogElements}
            </div>
            <div className={style.dialogs__messages}>
                {messagesElements}
                <div>
                    <textarea ref={addNewMsgText} 
                              onChange={onChangeHandler}
                              placeholder="Enter tour message" 
                              cols="30" rows="10" 
                              value={props.newMessageBody}></textarea>
                </div>
                <Button onClick={addMsgHandler} variant="outline-primary">Send</Button>
            </div>
            
        </div>
    )
};

export default Dialogs;
