import React from "react";
import {addMsgActionCreator, updateNewMessageActionCreator} from "../../redux/dialogsReducer";
import Dialogs from "./Dialogs";
import {connect} from "react-redux";

const mapStateToProps = (state) => {
    return {
        dialogsData: state.dialogsPage.dialogsData,
        messagesData: state.dialogsPage.messagesData,
        newMessageBody: state.dialogsPage.newMessageBody
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        updateNewMessageActionCreator: (newText) => {
            const action = updateNewMessageActionCreator(newText);
            dispatch(action);
        },
        addMsgActionCreator: (text) => {
            const action = addMsgActionCreator(text);
            dispatch(action);
        }        
    }
}

const DialogsContainer = connect(mapStateToProps,mapDispatchToProps)(Dialogs);

export default DialogsContainer;
