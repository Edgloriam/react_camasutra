import React from "react";
import style from '../Dialogs/Dialogs.module.css';
import { NavLink } from 'react-router-dom';

const DialogsItems = (props) => {
    let path = '/messages/' + props.id
    return (
        <div className={style.dialogs__item + ' ' + style.active} id={`dailogs_${props.id}`}>
            <NavLink to={path}>{props.name}</NavLink>
        </div>
    )
}

export default DialogsItems;
