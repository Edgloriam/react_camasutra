import React from "react";
import {addPostActionCreator, updateNewPostActionCreator} from "../../redux/profileReducer";
import Posts from "./Posts";
import {connect} from "react-redux";

const mapStateToProps = (state) => {
  return {
      postsData: state.profilePage.postsData,
      newMessage: state.profilePage.newMessage
  }  
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateNewPostText: (text) => {
            const action = updateNewPostActionCreator(text);
            dispatch(action);
        },
        addPostHandler: () => {
            const action = addPostActionCreator();
            dispatch(action);
        }
    }
}

const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(Posts);

export default PostsContainer;
