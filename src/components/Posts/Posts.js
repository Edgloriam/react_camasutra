import React from "react";
import style from './Posts.module.css';
import Post from "./Post/Post";
import {Button} from "react-bootstrap";

const Posts = (props) => {
    const postsElements = props.postsData
        .map(post => (<Post message={post.msg} key={`p_${post.id}`}/>))

    const newPostText = React.createRef();

    const addPostHandler = () => {
        props.addPostHandler();
    }
    const updateNewPostHandler = () => {
        let text = newPostText.current.value;
        props.updateNewPostText(text);
       
    }
    return (
        <div>
            <h1>My posts</h1>
            <div>
                <textarea ref={newPostText} name="addMsg" id="addMsg" cols="20" rows="3"
                          onChange={updateNewPostHandler}
                          value={props.newMessage}/>
            </div>
            <Button onClick={addPostHandler} variant="outline-primary">Add post</Button>
            <div className={style.posts}>
                {postsElements}
            </div>
        </div>
    )
};

export default Posts;
