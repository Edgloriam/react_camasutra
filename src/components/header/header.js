import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import logo from './logo192.png';
import classes from './Header.module.css'

const Header = () => {
    return (
        <header className={classes.header}>
            <Navbar expand="lg">
                <Navbar.Brand href="#home"> <img
                    src={logo}
                    height="30"
                    width="30"
                    className="d - inline - block aligt-top App-logo"
                    alt="Logo"
                /></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link className={classes.item} href="#home">Home</Nav.Link>
                        <Nav.Link className={classes.item} href="#link">About</Nav.Link>
                        <Nav.Link className={classes.item} href="#link">Contacts</Nav.Link>
                        <Nav.Link className={classes.item} href="#link">Blog</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </header>
    )
}

export default Header;
