import React from "react";
import { Nav } from "react-bootstrap";
import classes from './Side_panel.module.css'
import { NavLink } from "react-router-dom";

const SidePanel = () => {
    return (
        <Nav defaultActiveKey="/home" className="flex-column side_panel">
            <NavLink className={classes.item} activeClassName={classes.active} to="/profile">Profile</NavLink>
            <NavLink className={classes.item} activeClassName={classes.active} to="/messages">Messages</NavLink>
            <NavLink className={classes.item} activeClassName={classes.active} to="/music">Music</NavLink>
            <NavLink className={classes.item} activeClassName={classes.active} to="/users">Users</NavLink>
        </Nav>
    )
}

export default SidePanel;
