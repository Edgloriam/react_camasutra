import React from "react";
import style from '../Dialogs/Dialogs.module.css';

const Messages = (props) => {
    return (
        <div className={style.dialogs__messages_message}>
            {props.msg}
        </div>
    )
}


export default Messages;
