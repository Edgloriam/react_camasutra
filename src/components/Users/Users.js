import React from 'react';
import style from './Users.module.css';
import {Button} from "react-bootstrap";
import heart from './img/heart.svg'
const Users = (props) => {
    const toggleFollowState = (userID) => {
        props.toggleFollowState(userID);
    }
    
    const usersElements = props.users.map(user => {
        return (
            <div className={style.user} key={user.id}>
                <div className={style.user__avatar}>
                    <div>{user.name}</div>
                    <img src={user.avatarUrl} alt=""/>
                </div>
                <div className={style.user__follow_btn}>
                    <Button onClick={() => toggleFollowState(user.id)} variant={user.followed ? 'danger' : 'success'}>
                        {user.followed ? 'Unfollow' : 'followed'}
                    </Button>
                </div>
                <div className={style.user__info}>
                    <div>City: {user.city}</div>
                    <div>Slogan: {user.slogan}</div>
                    <div className={style.user__likes} onClick={() => props.addLike(user.id)}>
                        <img src={heart} alt=""/>
                        <span>{user.likes > 0 ? user.likes : '' }</span>
                    </div>
                </div>
            </div>
        )
    });
    return (usersElements)
};

export default Users;
