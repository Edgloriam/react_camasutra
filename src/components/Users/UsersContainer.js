import React from 'react';
import {connect} from "react-redux";
import Users from './Users';
import {toggleFollowStateAC, addLikeAC} from "../../redux/usersReducer";

const mapStateToProps = (state) => {
    return {
        users: state.usersPage.users
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        toggleFollowState: (userID) => {
            const action = toggleFollowStateAC(userID);
            dispatch(action);
        },
        addLike: (userID) => {
            const action = addLikeAC(userID);
            dispatch(action)
        }
    }
}

const UsersContainer = connect(mapStateToProps, mapDispatchToProps)(Users);

export default UsersContainer;
