const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST = 'UPDATE-NEW-POST';

const initialState = {
    postsData: [
        {id: 1, msg: 'Yo', likes: 6},
        {id: 2, msg: 'Yoooo', likes: 5},
        {id: 3, msg: 'Yoooo', likes: 1},
        {id: 4, msg: 'Yoooooooooo', likes: 6},
        {id: 5, msg: 'Yoooooooooooooo', likes: 2},
        {id: 6, msg: 'Yo-o', likes: 6},
    ],
    newMessage: "Fuck!"
}

const profileReducer  = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            const postsCount = state.postsData.length;
            const newText = state.newMessage;
            return {
                ...state,
                postsData: [...state.postsData, {
                    id: postsCount + 1,
                    msg: newText,
                    likes: 15
                }]
            };
        case UPDATE_NEW_POST:
            return {
                ...state,
                newMessage: action.text
            };
        default:
            return {...state};
    }
};
export const addPostActionCreator = () => ({  type: ADD_POST });

export const updateNewPostActionCreator = (text) => {
    return {
        type: UPDATE_NEW_POST,
        text
    }
};
export default profileReducer;
