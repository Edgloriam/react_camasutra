import {combineReducers, createStore} from "redux";
import profileReducer from "./profileReducer";
import sidePanelReducer from "./sidePanelReducer";
import dialogsReducer from "./dialogsReducer";
import testReducer from "./testReducer";
import usersReducer from "./usersReducer";

const reducers = combineReducers({
    profilePage: profileReducer,
    dialogsPage: dialogsReducer,
    sidePanel: sidePanelReducer,
    usersPage: usersReducer,
    test: testReducer
})

let store = createStore(reducers);
export default store;
