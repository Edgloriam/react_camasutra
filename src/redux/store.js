import profileReducer from "./profileReducer";
import dialogsReducer from "./dialogsReducer";
import sidePanelReducer from "./sidePanelReducer";

let store = {
    _state: {
        messagePage:{
            dialogsData: [
                { id: 1, name: 'Alex', messages: [ 1, 2, 3 ] },
                { id: 2, name: 'Leha', messages: [ 4, 5, 6 ] },
                { id: 3, name: 'Lena', messages: [ 7, 8, 9, 10 ] },
                { id: 4, name: 'Sasha', messages: [ 11, 12, 13, 14, 15, 16 ] },
                { id: 5, name: 'Dima', messages: [ 17, 18, 19, 20, 21, 22 ] },
                { id: 6, name: 'Alina', messages: [ 23, 24, 25, 26, 27, 28 ]  }
            ],
            messagesData: [
                {id: 1, msg: 'Alex    asdasdasdsad  ad asd asd asd sad '},
                {id: 2, msg: 'Alex     sadasdasdasdas dasd asd'},
                {id: 3, msg: 'Alex    dasd3432r2f243f23f'},
                {id: 4, msg: 'Leha   Safdfsdfsdf sha'},
                {id: 5, msg: 'Leha   Dimsd f sdf sdfa'},
                {id: 6, msg: 'Leha   Alin sdf sdf sdfsdf a'},
                {id: 7, msg: 'Lena    sadasdasdasdas dasd asd'},
                {id: 8, msg: 'Lena   dasd3432r2f243f23f'},
                {id: 9, msg: 'Lena   Safdfsdfsdf sha'},
                {id: 10, msg: 'Lena   Dimsd f sdf sdfa'},
                {id: 11, msg: 'Sasha asdasdasdsad  ad asd asd asd sad '},
                {id: 12, msg: 'Sasha  sadasdasdasdas dasd asd'},
                {id: 13, msg: 'Sasha dasd3432r2f243f23f'},
                {id: 14, msg: 'Sasha Safdfsdfsdf sha'},
                {id: 15, msg: 'Sasha Dimsd f sdf sdfa'},
                {id: 16, msg: 'Sasha Alin sdf sdf sdfsdf a'},
                {id: 17, msg: 'asdasdasdsad  ad asd asd asd sad '},
                {id: 18, msg: ' sadasdasdasdas dasd asd'},
                {id: 19, msg: 'dasd3432r2f243f23f'},
                {id: 20, msg: 'Safdfsdfsdf sha'},
                {id: 21, msg: 'Dimsd f sdf sdfa'},
                {id: 22, msg: 'Alin sdf sdf sdfsdf a'},
                {id: 23, msg: 'asdasdasdsad  ad asd asd asd sad '},
                {id: 24, msg: ' sadasdasdasdas dasd asd'},
                {id: 25, msg: 'dasd3432r2f243f23f'},
                {id: 26, msg: 'Safdfsdfsdf sha'},
                {id: 27, msg: 'Dimsd f sdf sdfa'},
                {id: 28, msg: 'Alin sdf sdf sdfsdf a'},
            ],
            newMessageBody: ''
        },
        profilePage: {
            postsData: [
                {id: 1, msg: 'Yo', likes: 6},
                {id: 2, msg: 'Yoooo', likes: 5},
                {id: 3, msg: 'Yoooo', likes: 1},
                {id: 4, msg: 'Yoooooooooo', likes: 6},
                {id: 5, msg: 'Yoooooooooooooo', likes: 2},
                {id: 6, msg: 'Yo-o', likes: 6},
            ],
            newMessage: "Fuck!"
        },
        sidePanelPage: {}
    },
    _callSubcriber () {
        console.log('Nothing to show');
    },
    
    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubcriber = observer;
    },
    
    dispatch(action) {
        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.messagePage = dialogsReducer(this._state.messagePage, action);
        this._state.sidePanelPage = sidePanelReducer(this._state.sidePanelPage, action);
        this._callSubcriber(this);
        
    }
};


export default store;

