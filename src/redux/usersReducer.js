const TOGGLE_FOLLOW_STATE= 'TOGGLE_FOLLOW_STATE';
const ADD_LIKE= 'ADD_LIKE';

const initialState = {
    users: [
        {id:1, name: 'Alex', likes: 0, followed: true, avatarUrl: 'https://avatars.mds.yandex.net/get-kinopoisk-image/1704946/069b3f8f-c37d-445e-bc2f-20df3b2c97d8/orig', city: 'Dnepr', slogan: 'Im a boss'},
        {id:2, name: 'Alina', likes: 0, followed: false, avatarUrl: 'https://avatars.mds.yandex.net/get-kinopoisk-image/1704946/069b3f8f-c37d-445e-bc2f-20df3b2c97d8/orig', city: 'Dnepr', slogan: 'Im a bossIm a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  '},
        {id:3, name: 'Jack', likes: 0, followed: false, avatarUrl: 'https://avatars.mds.yandex.net/get-kinopoisk-image/1704946/069b3f8f-c37d-445e-bc2f-20df3b2c97d8/orig', city: 'Dnepr', slogan: 'Im a bossIm a boss.  Im a boss.  '},
        {id:4, name: 'Sara', likes: 0, followed: true, avatarUrl: 'https://avatars.mds.yandex.net/get-kinopoisk-image/1704946/069b3f8f-c37d-445e-bc2f-20df3b2c97d8/orig', city: 'Dnepr', slogan: 'Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  Im a boss.  '}
    ]
}

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_FOLLOW_STATE:
            return {
                ...state,
                users: state.users.map(user => {
                    if(user.id === action.userID){
                        return {
                            ...user,
                            followed: !user.followed
                        }
                    }
                    return user;
                })
            }
        case ADD_LIKE:
            return {
                ...state,
                users: state.users.map(user => {
                    if (user.id === action.userID) {
                        return {
                            ...user,
                            likes: user.likes + 1
                        }
                    }
                    return user;
                })
            }
        default:
            return state;
    }
};

export const toggleFollowStateAC = (userID) => ({type: TOGGLE_FOLLOW_STATE, userID});
export const addLikeAC = (userID) => ({type: ADD_LIKE, userID});

export default usersReducer;
 
