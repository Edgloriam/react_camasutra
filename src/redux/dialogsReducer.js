const ADD_MSG = 'ADD-MSG';
const UPDATE_NEW_MSG = 'UPDATE_NEW_MSG';

const initialState = {
    dialogsData: [
        { id: 1, name: 'Alex', messages: [ 1, 2, 3 ] },
        { id: 2, name: 'Leha', messages: [ 4, 5, 6 ] },
        { id: 3, name: 'Lena', messages: [ 7, 8, 9, 10 ] },
        { id: 4, name: 'Sasha', messages: [ 11, 12, 13, 14, 15, 16 ] },
        { id: 5, name: 'Dima', messages: [ 17, 18, 19, 20, 21, 22 ] },
        { id: 6, name: 'Alina', messages: [ 23, 24, 25, 26, 27, 28 ]  }
    ],
    messagesData: [
        {id: 1, msg: 'Alex    asdasdasdsad  ad asd asd asd sad '},
        {id: 2, msg: 'Alex     sadasdasdasdas dasd asd'},
        {id: 3, msg: 'Alex    dasd3432r2f243f23f'},
        {id: 4, msg: 'Leha   Safdfsdfsdf sha'},
        {id: 5, msg: 'Leha   Dimsd f sdf sdfa'},
        {id: 6, msg: 'Leha   Alin sdf sdf sdfsdf a'},
        {id: 7, msg: 'Lena    sadasdasdasdas dasd asd'},
    ],
    newMessageBody: ''
};

const dialogsReducer  = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MSG:
            const msgCount = state.messagesData.length;
            return {
                ...state,
                messagesData: [...state.messagesData, {
                    id: msgCount + 1,
                    msg: action.text,
                }],
            };
        case UPDATE_NEW_MSG:
            const newMsgText = action.text;
            return {
                ...state,
                newMessageBody: newMsgText
            };
        default:
            return {
                ...state
            };
    }
};
export const addMsgActionCreator = (text) => {
    return {
        type: ADD_MSG,
        text
    }
};
export const updateNewMessageActionCreator = (text) => {
    return {
        type: UPDATE_NEW_MSG,
        text
    }
};

export default dialogsReducer;
